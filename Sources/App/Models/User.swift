import FluentPostgreSQL
import Vapor
import Foundation

final class User: Codable {
    var id: UUID?
    var name: String
    var createdAt: Date?
    var updatedAt: Date?
    
    static let createdAtKey: CreatedAtKey = \User.createdAt
    static let updatedAtKey: UpdatedAtKey = \User.updatedAt
    
    init(name: String){
        self.name = name
    }
}

extension User: PostgreSQLUUIDModel {}
extension User: Content {}
extension User: Parameter {}
extension User: Migration {}
extension User: Timestampable {}
