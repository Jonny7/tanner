import FluentPostgreSQL
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first
    try services.register(FluentPostgreSQLProvider())

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    /// middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // configure PostgreSQL Database from environment
    let username = "vapor-test"
    let database = "vapor-development"
    let password = "password"
    let port = 5433

    
    var databases = DatabasesConfig()
    let dbConfig = PostgreSQLDatabaseConfig(hostname: "localhost", port: port, username: username, database: database, password: password)
    let dbConnection = PostgreSQLDatabase(config: dbConfig)
    databases.add(database: dbConnection, as: .psql)
    services.register(databases)

    /// Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .psql)
    services.register(migrations)

}
